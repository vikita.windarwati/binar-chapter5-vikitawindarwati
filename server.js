const express = require('express');
const app = express();
const router = require('./routing')
const path = require('path')
const {internalServerError, notFound} = require('./middleware');


//load css
app.use(express.static('404'))
app.use('/chapter3', express.static(path.join(__dirname, 'chapter-3')))
app.use('/chapter4', express.static(path.join(__dirname, 'chapter-4')))


//app.set json print style
app.set('json spaces', 4);
//routing
app.use(router);

// error 500
app.use(internalServerError);
// 404
app.use(notFound)

//listen req
app.listen(5000, ()=> {
    console.log('on port 5000')
});

