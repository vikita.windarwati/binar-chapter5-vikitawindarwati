const players=Array.from(document.querySelectorAll(".p1"));   //make array from nodelist
const coms=Array.from(document.querySelectorAll(".com"));
const refresh=document.querySelector(".refresh-size")
const changeImage=document.querySelector(".vs-wrapper")

const playerParent=document.querySelector(".wrapper")
const comParent=document.querySelector(".comWrap")




playerParent.addEventListener('mouseover', styleThere)
function styleThere() {
    let e=event.target;
    if (e.classList.contains('wrapper')) {     //need to do this because stopPropagation() doesn't work
        e.classList.remove('pic-selection');   //or am I mistaken in using the stopPropagation()?
        e.style.cursor='';
    }
    else if (e.classList.contains('p1')) {
        e.style.cursor='pointer';
        e.classList.add('pic-selection');
    }
}
playerParent.addEventListener('mouseout', styleGone) 
function styleGone() {
    let e = event.target;
    e.classList.remove('pic-selection');
}


playerParent.addEventListener('click', selected, {once: true});
function selected() {
    //remove the event listener mouseover and out
    playerParent.removeEventListener('mouseover', styleThere)
    playerParent.removeEventListener('mouseout', styleGone)
    let e=event.target;
    e.style.cursor='';

    //preventing <div> parent from activating *brute force, need to be fixed*
    if (e.classList.contains('wrapper')) {
        playerParent.removeEventListener('click', selected, {once: true});
        e.classList.remove('pic-selection');
    }

    //add style on click and get index of rockpaperscissor by the click
    e.classList.add('pic-selection');
    let player=players.indexOf(e);
    
    //randomizing com side, outputting and getting index from 'acak' 
    let rand=Math.floor(Math.random() * coms.length);
    
    // coms.forEach(function(newCom) {
        coms[rand].classList.add('pic-selection');
    // });

    //function for showing player+com win and draw
    function imgInsertSrc(source) {
        var img=document.createElement("img");
        img.className="position";
        img.src=source;
        // img1.src="assets/player-win.png";
        changeImage.replaceChild(img, changeImage.childNodes[1]); //kelemahan: kalau diedit htmlnya bisa unrecognize. *cari cara lain ke depannya*
    } 
    
    //object for comparison
    let win = {
        "0": 2,
        "1": 0,
        "2": 1,
    }
    
    //nama element di console.log
    let nameList= ["BATU", "KERTAS", "GUNTING"];
    
    //seri conditional
    if (player==rand) {
        console.log("PLAYER: " + nameList[player])
        console.log("COM: " + nameList[rand])
        console.log("SERI")
        imgInsertSrc("assets/rectangle_draw2.png");
    }
    
    //win and lose conditional
    for (let key in win) {
        if (player==key && rand==win[key]) {
            //test output
            console.log("PLAYER: " + nameList[player]);
            console.log("COM: " + nameList[rand]);
            console.log("PLAYER MENANG");
            imgInsertSrc("assets/player-win.png");
            
        }
        if(player==win[key] && rand==key) {
            console.log("PLAYER: " + nameList[player]);
            console.log("COM: " + nameList[rand]);
            console.log("COMPUTER MENANG");
            imgInsertSrc("assets/com-win.png");
        }

        
    }
    
}
//want to add alert
//refresh
refresh.addEventListener('click', ()=>{window.location.reload()});        

//kekurangan:   -di drag di div wrapper masih bereaksi walaupun sudah di set if ketika jendela window dikecilkan
//              -masih kurang responsive saat window dikecilkan