const internalServerError = (err, req, res, next) => {
    console.error(err)
    res.status(500).json({
        status: 'fail',
        errors: err.message
    })
    next();
}

const notFound = (req, res, next) => {
    res.status(404).sendFile('./404/404.html', { root: __dirname });
    // res.status(404).send("URL not found");
    // next();
}

module.exports = {internalServerError, notFound};