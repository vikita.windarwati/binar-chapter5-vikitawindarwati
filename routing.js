const express = require('express');
const users = require('./users_API/users.json');
const router = express.Router();

//chapter 3
router.get('/chapter3', (req, res)=>{
    res.sendFile('./chapter-3/index.html', { root: __dirname });
});

//chapter 4
router.get('/chapter4', (req, res)=>{
    res.sendFile('./chapter-4/game.html', { root: __dirname });
});

//user static
router.get('/users', (req, res)=>{

    function compare(a, b) {
        // Use toUpperCase() to ignore character casing
        const nameA = a.name.toUpperCase();
        const nameB = b.name.toUpperCase();
      
        let comparison = 0;
        if (nameA > nameB) {
          comparison = 1;
        } else if (nameA < nameB) {
          comparison = -1;
        }
        return comparison;
    }
    // users.users.sort(compare);
    res.json(users.users.sort(compare));
});


router.get('/users/:id', (req, res)=>{
    // note to self : ditambahkan id, tapi belum ketemu fungsi biar id bisa diinput ke atas
    // unshift dan array method lain tidak bisa dipakai
    let newUsers=JSON.parse(JSON.stringify(users)); //note: deep copy. original doesn't change
    let i;
    for(i=0; i<newUsers.users.length; i++) {
        let id='id: ';
        value=i+1;
        newUsers.users[i].id=value;
    }
    const searchId =newUsers.users.find(i => i.id=== +req.params.id)   
    res.json(searchId)
});

module.exports = router;